
// Auth controller

const { fbAuth, fbAdminAuth, googleProvider } = require('../../../configs/db.config')

// const val HEADER_AUTH_LABEL = "Authorization"
// const val HEADER_AUTH_BEARER = "Bearer"

const verifyTkn = (req, res, next) => {
    
    try {
        // const headerAuth = req.headers['authorization'] // Authorization
        // console.log('headerAuth ::; ', headerAuth)

        // const bearer = headerAuth.split(' ') // Bearer
        // const bearerToken = bearer[1] // 123321123321

        // res.json({ hh: headerAuth, bearer: bearer, bearerToken: bearerToken })

        const tkn = req.body.tkn

        fbAdminAuth.verifyIdToken(tkn)
            .then((decodedToken) => {
              const uid = decodedToken.uid
            //   res.status(201).json({ uid:  uid, decodedToken: decodedToken })
              res.status(201).json({ user: decodedToken })
            // req.userUid = uid
            // next()
            }).catch((error) => {
                console.log('error ::; ', error)
                if(error.code === 'auth/id-token-expired') {
                    res.status(401).json({ error:  error })
                }
                res.status(400).json({ error:  error })
            })
    } catch (error) {
        console.log('error ::; ', error)
        res.status(400).json({ error:  error })
    }

}

const userIsVeryfied = (req, res, next) => {
    res.status(200).json({ msg: "Ok!", userId: req.userUid })
}

const createAuth = (req, res, next) => {

    try {

        const userCreateAuth = {
            email: req.body.email,
            password: req.body.password,
            displayName: req.body.display_name
        }

        fbAuth.createUserWithEmailAndPassword(userCreateAuth.email, userCreateAuth.password)
            .then((response) => {
                // console.info('response ::; ', response)
                res.status(201).json({ user:  response.user.email, userRes: response })
            }) 
            .catch((error) => {
                const errorCode = error.code
                const errorMessage = error.message
                if (errorCode === 'auth/wrong-password') {
                    res.status(404).json({ message: 'Wrong password.' })
                } else {
                    res.status(400).json({ message: errorMessage })
                }
            })
        
    } catch (error) {
        res.status(500).json({ serverError: error })
    }
}

const loginAuth = (req, res, next) => {

    try {
        
        const userLoginAuth = {
            email: req.body.email,
            password: req.body.password
        }

        fbAuth.signInWithEmailAndPassword(userLoginAuth.email, userLoginAuth.password)
        .then(responseUser => {
            // console.info('response ::; ', response)
            
            // res.status(200).json({ user:  response.user.email, tkn: response.user.refreshToken })

            fbAuth.currentUser.getIdToken(false)
            .then((idToken) => {
                res.status(200).json({ responseUser })
             })
             .catch(function (error) {
                res.status(400).json({ message: errorMessage })
             })

        })
        .catch(error => {
            const errorCode = error.code
            const errorMessage = error.message
            if (errorCode === 'auth/wrong-password') {
                res.status(404).json({ message: 'Wrong password.' })
            } else {
                res.status(400).json({ message: errorMessage })
            }
        })
        
    } catch (error) {
        res.status(500).json({ serverError: error })
    }

}

const logoutAuth = (req, res, next) => {

    try {

        fbAuth.signOut()
        .then(response => {
            // console.info('response ::; ', response)
            res.status(200).json({ user:  response })
        })
        .catch(error => {
            const errorCode = error.code
            const errorMessage = error.message
            if (errorCode === 'auth/wrong-password') {
                res.status(404).json({ message: 'Wrong password.' })
            } else {
                res.status(400).json({ message: errorMessage })
            }
        })
        
    } catch (error) {
        res.status(500).json({ serverError: error })
    }

}

const loginAuthGoogle = (req, res, next) => {

    try {

        fbAuth.signInWithPopup(googleProvider)
        .then(response => {
            console.info('response ::; ', response)
            res.status(200).json({ user:  response })
        })
        .catch(error => {
            const errorCode = error.code
            const errorMessage = error.message
            if (errorCode === 'auth/wrong-password') {
                res.status(404).json({ message: 'Wrong password.' })
            } else {
                res.status(400).json({ message: errorMessage })
            }
        })
        
    } catch (error) {
        res.status(500).json({ serverError: error })
    }

}

const authControllers = {
    createAuth,
    loginAuth,
    logoutAuth,
    loginAuthGoogle,
    verifyTkn,
    userIsVeryfied
}

module.exports = authControllers

// "userprofile": {
//     "data":{
//     "id": 1,
//     "user_name": "Felipe",
//     "user_identifier": "@felipe",
//     "city": "Belo horizonte",
//     "country": "Brazil",
//     "email":"felipe@urmnz.com",
//     "phone":"+351910884718",
//     "birthday": 123413,
//     "user_type": 0,
//     "is_pro":false,
//     "payd_options": null,
//     "height": 12314,
//     "weight": 3141,
//     "sport_interest": ["tenis","soccer"]
//         },
//     "response":{
//     "status":"OK",
//     "code":"00APP",
//     "msg":"OK"
//         }
//       }
//     }


// {"data":[ ], "meta":{}, "response": status:"OK", "code":AB1231, "msg": OK+ }