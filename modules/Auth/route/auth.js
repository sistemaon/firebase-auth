
// Auth route

const express = require('express')
const router = express.Router()

const Auth = require('../controller/auth')

// router endpoint to create user auth with email and password
router.post('/user', Auth.createAuth)

// router endpoint to login user auth with email and password
router.post('/user/signin', Auth.loginAuth)

// router endpoint to logout user auth with email and password
router.post('/user/signout', Auth.logoutAuth)

// router endpoint to google provider login user auth
router.get('/user/gsignin', Auth.loginAuthGoogle)

// router endpoint to google provider login user auth
// router.use(Auth.verifyTkn)
router.post('/user/tkn', Auth.verifyTkn)

module.exports = router