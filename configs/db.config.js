
const firebase = require('firebase')
const admin = require('firebase-admin')

// configuration environment from configs file environment.config.example.js
const environmentConfig = require('./environment.config') // environment.config.example.js

require('firebase/auth')
require('firebase/database')

const serviceAccount = require('../configs/serviceAccountKey.json') // serviceAccountKey.example.json
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: environmentConfig.DATABASE_URL
})

const firebaseConfig = {
  apiKey: environmentConfig.API_KEY,
  authDomain: environmentConfig.AUTH_DOMAIN,
  databaseURL: environmentConfig.DATABASE_URL,
  projectId: environmentConfig.PROJECT_ID,
  storageBucket: environmentConfig.STORAGE_BUCKET,
  messagingSenderId: environmentConfig.MESSAGING_SENDER_ID,
  appId: environmentConfig.APP_ID,
  measurementId: environmentConfig.MEASUREMENT_ID
}

// Initialize Firebase
const fbApp = firebase.initializeApp(firebaseConfig)
const fbAuth = fbApp.auth()
const fbAdminAuth = admin.auth()

const fbDb = fbApp.database()

// Firebase auth provider
const googleProvider = new firebase.auth.GoogleAuthProvider()

module.exports = {
    fbAuth,
    fbAdminAuth,
    fbDb,
    googleProvider
}
